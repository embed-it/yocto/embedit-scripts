#!/bin/sh

# Initialize env variables
ROOT=$(readlink -f ${PWD})
TEMPLATECONF=$ROOT/src/meta-bsp/meta-bsp-ol/conf/templateconf
unset DEVMODE_EN # bool, to enable Yocto development flags
unset BUILDDIR   # path, yocto build directory

show_help() 
{
	printf "Usage: embedit-init-env.sh [options]\n\n"
	printf "Options:\n"
	printf " -h, --help\t\tshow this help message and exit\n"
	printf " -d, --dev\t\tenable Yocto development flags\n"
	printf " -b BUILDDIR, --build-dir=BUILDDIR\n\t\t\tset the build directory\n" 
}

option_value_exists()
{
	if [ "$1" -gt "1" ]; then return 0; fi
	printf "option requires additional parameters\n"
	exit 1
}

while [ "$#" -gt "0" ]
do
opt="$1"
case $opt in		
	-h|--help)
		show_help
		exit 0
	;;
	-d|--dev)
		DEVMODE_EN=true
		shift
	;;
	-b)
		option_value_exists $#
		BUILDDIR="$2"
		shift
		shift
	;;
	--build-dir=*)
		BUILDDIR="${opt#*=}"
		shift
	;;
	*)
		echo Unknown option: $opt
		show_help
		exit 1
	;;
esac
done

if [ "$DEVMODE_EN" =  "true" ] ; then
	TEMPLATECONF=$TEMPLATECONF-dev
fi

BUILDDIR="${BUILDDIR:-$ROOT/build}"

# source oe-init-build-env of yocto
TEMPLATECONF=$TEMPLATECONF source $ROOT/src/poky/oe-init-build-env $BUILDDIR
